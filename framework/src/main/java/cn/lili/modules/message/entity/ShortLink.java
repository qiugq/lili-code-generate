package cn.lili.modules.message.entity;

import cn.lili.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Chopper
 */
@Data
@Entity
@Table(name = "li_short_link")
@TableName("li_short_link")
@ApiModel(value = "短链接")
public class ShortLink extends BaseEntity {

    private static final long serialVersionUID = 1L;

}